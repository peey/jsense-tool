package in.edu.iiitd.pag.jsense.vss;

import in.edu.iiitd.pag.jsense.Helpers;
import in.edu.iiitd.pag.jsense.snippets.Snippet;
import in.edu.iiitd.pag.jsense.snippets.Snippets;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DocumentVectorsSingletonTest {

  static class Stub extends DocumentVectorsSingleton {
    final public VectorSpace<String> vecspace = new VectorSpace<String>();

    static HashMap<Snippet, VectorSpace.Vector> vectorCache = new HashMap<>();
    public HashMap<Snippet, VectorSpace.Vector> getCache() {
      return vectorCache;
    }

    public VectorSpace.Vector makeVector(Snippet s) throws IOException {
      String mockVectorText;
      switch (s.identifier) {
        case "a":
          mockVectorText = "cat cat ro ro bat bat";
          break;
        case "b":
          mockVectorText = "football enthusiasts cat";
          break;
        default:
          mockVectorText = "cat ro bat";
      }
      return Helpers.makeVectorTokenizingByWhitespace(mockVectorText, vecspace);
    }
  }

  /*
  // important so that internal key numbers are the same for each dimensions in the vectors
  // constructed here and in the vectors constructed in the program by DocumentVectorsSingleton classes
  static VectorSpace vecspace = DocumentVectorsSingleton.vecspace;

  @BeforeEach
  void setUp() {
    Snippets.restrict = false;
  }
  */

  //used for next 4 tests
  static Stub stub = new Stub();

  @Test
  void makeVectorTokenizingByWhitespace() {
    VectorSpace.Vector v;
    String s;

    s = "cat mat bat";
    v = Helpers.makeVectorTokenizingByWhitespace(s, stub.vecspace);
    assertEquals(v.toString(), "Vector[ (bat, 1.0) (cat, 1.0) (mat, 1.0) ]");

    s = "cat mat bat cat";
    v = Helpers.makeVectorTokenizingByWhitespace(s, stub.vecspace);
    assertEquals(v.toString(), "Vector[ (bat, 1.0) (cat, 2.0) (mat, 1.0) ]");

    s = "cat mat \n bat cat";
    v = Helpers.makeVectorTokenizingByWhitespace(s, stub.vecspace);
    assertEquals(v.toString(), "Vector[ (bat, 1.0) (cat, 2.0) (mat, 1.0) ]");

    s = "cat mat bat cat=";
    v = Helpers.makeVectorTokenizingByWhitespace(s, stub.vecspace);
    assertEquals(v.toString(), "Vector[ (bat, 1.0) (cat, 1.0) (cat=, 1.0) (mat, 1.0) ]");
  }

  // these are used for the next 3 tests
  static VectorSpace.Vector va, vb, vc;
  static {
    try {

      va = stub.makeVector(new Snippet(new File(""), "a"));
      vb = stub.makeVector(new Snippet(new File(""), "b"));
      vc = stub.makeVector(new Snippet(new File(""), "c"));

    } catch (Exception e) {
      // ignore, will never happpen
      assert false : "Well, this shouldn't happen";
    }
  }

  @Test
  void makeVector() throws Exception {
    VectorSpace.Vector ta = stub.vecspace.new Vector();
    VectorSpace.Vector tb = stub.vecspace.new Vector();
    VectorSpace.Vector tc = stub.vecspace.new Vector();

    ta.set("cat", 2);
    ta.set("ro", 2);
    ta.set("bat", 2);

    assert ta.equals(va);

    tb.set("football", 1);
    tb.set("enthusiasts", 1);
    tb.set("cat", 1);

    assert tb.equals(vb);

    tc.set("football", 1);
    tc.set("enthusiasts", 1);
    tc.set("cat", 1);

    assert tc.equals(vb);
  }

  @Test
  void getDocumentFrequency() {
    HashMap<String, Integer> ht = stub.getDocumentFrequency(Arrays.asList(va, vb, vc));
    assert ht.get("cat") == 3;
    assert ht.get("ro") == 2;
    assert ht.get("bat") == 2;
    assert ht.get("football") == 1;
    assert ht.get("enthusiasts") == 1;
  }

  @Test
  void getIDF() {
    HashMap<String, Integer> df = stub.getDocumentFrequency(Arrays.asList(va, vb, vc));
    HashMap<String, Double> idf = stub.getIDF(df, 3);
    //formula: log(total / actual)
    assertEquals(Math.log(3.0/3.0), idf.get("cat"),0.001);
    assertEquals(Math.log(3.0/2.0), idf.get("ro"),0.001);
    assertEquals(Math.log(3.0/2.0), idf.get("bat"),0.001);
    assertEquals(Math.log(3.0/1.0), idf.get("football"),0.001);
    assertEquals(Math.log(3.0/1.0), idf.get("enthusiasts"),0.001);
  }
}
