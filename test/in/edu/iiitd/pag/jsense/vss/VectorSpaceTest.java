package in.edu.iiitd.pag.jsense.vss;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Hashtable;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VectorSpaceTest {

  static VectorSpace vecspace = new VectorSpace<String>();

  static VectorSpace.Vector v, v1, v2, v3, v4, v5, vx, vy, vz;

  @BeforeEach
  void setUp() {
    v = vecspace.new Vector();
    v1 = vecspace.new Vector();
    v2 = vecspace.new Vector();
    v3 = vecspace.new Vector();
    v4 = vecspace.new Vector();
    v5 = vecspace.new Vector();
    vx = vecspace.new Vector();
    vy = vecspace.new Vector();
    vz = vecspace.new Vector();
  }

  @Test
  void gettersAndSetters() {
    v.set("a", 1);
    assert v.get("a") == 1.0;
    v.set("a", 2.5);
    assert v.get("a") == 2.5;
  }

  @Test
  void norm2() {
    v.set("a", 1);
    v.set("b", 1);
    assert v.norm2() == Math.sqrt(2);
  }

  @Test
  void normalizedVectors() {
    v.set("a", 1);
    v.set("b", 1);
    vx = v.normalize();
    assert vx.get("a") == (float)1/Math.sqrt(2);
    assert vx.get("b") == (float)1/Math.sqrt(2);
    assertEquals(1.0, vx.norm2(), 0.001);
  }

  @Test
  void stringRepresentation() {
    v.set("a", 1);
    v.set("b", 1);

    assertEquals(v.toString(), "Vector[ (a, 1.0) (b, 1.0) ]");
  }

  @Test
  void equality() {
    v1.set("a", 1);
    v1.set("b", 1);

    v2.set("a", 1);
    v2.set("b", 1);

    assert v1.equals(v2);

    v3 = v1.normalize();
    v4.set("a", 1/Math.sqrt(2));
    v4.set("b", 1/Math.sqrt(2));

    assert v3.equals(v4);

    v1.set("a", 1);
    v1.set("b", 1);

    v2.set("a", 0.99);
    v2.set("b", 1);

    assert v1.equals(v2, 0.02);

  }

  @Test
  void weighted() {
    v.set("a", 1);
    v.set("b", 2.5);
    v.set("c", 8.9);

    Hashtable<String, Double> weights = new Hashtable<>();
    weights.put("a", 8.5);
    weights.put("b", 3.0);
    weights.put("c", 1.0/8.9);

    vx.set("a", 8.5);
    vx.set("b", 7.5);
    vx.set("c", 1.0);


    assert v.weighted(weights).equals(vx, 0.001);
  }
}
