package in.edu.iiitd.pag.jsense.representations;

import com.sun.source.util.TreePath;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import org.checkerframework.javacutil.AbstractTypeProcessor;

/**
 * Intercepts processing environment and makes sure AbstractTypeProcessor has run. The processingEnv is required to give
 * it as an argument to checker framework's CFGBuilder. CFGBuilder also expects AbstractTypeProcessor to have been run.
 * @author Peeyush Kushwaha
 */
@SupportedAnnotationTypes("*")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class AlmostEmptyProcessor extends AbstractTypeProcessor {
  public static ProcessingEnvironment processingEnv;

  @Override
  public synchronized void init(ProcessingEnvironment env) {
    super.init(env);
    processingEnv = env;
    //System.out.println("Processing env intercepted");
  }

  @Override
  public void typeProcess(TypeElement element, TreePath tree) { /* no-op */ }

}
