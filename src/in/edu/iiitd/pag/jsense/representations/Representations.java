package in.edu.iiitd.pag.jsense.representations;

import spoon.Launcher;
import spoon.reflect.code.*;
import spoon.reflect.declaration.*;
import spoon.reflect.reference.CtTypeReference;
import spoon.reflect.visitor.filter.TypeFilter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * We're using INRIA's Spoon library (available at <a href="https://github.com/INRIA/spoon">github.com/INRIA/spoon</a>)
 * to extract information from source's AST. Please see spoon's reference for full details on usage. I've also provided
 * a brief idea about what code is doing where needed.
 * @author Peeyush Kushwaha
 */
public class Representations {
  /**
   * Utilities local to the representations class
   * @author Peeyush Kushwaha
   */
  static class Util {
    // These filters are used to get child elements of a certain kind
    private static TypeFilter ifStatementFilter = new TypeFilter<CtIf>(CtIf.class);
    private static TypeFilter returnStatementFilter = new TypeFilter(CtReturn.class);

    /**
     * Considers it a child even if it's separated by just blocks
     * @param list
     * @param parent
     * @param <T>
     * @return
     */
    public static <T extends CtElement> List<T> filterInImmidiateChildren(List<T> list, CtElement parent) {
      return list.stream().filter(x -> isChild(x, parent)).collect(Collectors.toList());
    }

    /**
     * Get all the direct children of an element of any type
     * @param e element
     * @return List of child elements
     */
    public static List<CtElement> getAllDirectChildren(CtElement e) {
      return Util.filterInDirectChildren(e.getElements(new TypeFilter<>(CtElement.class)), e);
    }

    /**
     * Gets a list of direct children of a certain type. A direct child is an element whose parent is the provided element.
     * @param list
     * @param parent
     * @param <T> element type of the child elements
     * @return List of children of type T
     */
    public static <T extends CtElement> List<T> filterInDirectChildren(List<T> list, CtElement parent) {
      return list.stream().filter(x -> x.getParent() == parent).collect(Collectors.toList());
    }

    /**
     * @param child
     * @param parent
     * @return true if child is a decendant of parent (optionally separated by 0 or more blocks)
     */
    public static boolean isChild(CtElement child, CtElement parent) {
      if (child.getParent() == parent) {
        return true;
      } else if (child.getParent() instanceof CtBlock) {
        CtElement e = child.getParent();
        while (e instanceof CtBlock) {
          e = e.getParent();
          if (parent == e) {
            return true;
          }
        }
      }
      return false;
    }

    public static CtMethod getParentMethod(CtElement e) {
      return e.getParent(CtMethod.class);
    }
  }

  /**
   * same as operator.getKind().name() for CtBinaryOperator elements
   * LT: <, GT: >, LE: <=, GE: >=, EQ: ==, NE: !=
   */
  public static List<String> relationalOperators = List.of("LT", "GT", "LE", "GE", "EQ", "NE");

  /**
   * This representation returns names of relational operators which occur in the code within a loop or an branch construct.
   * These are prefixed by either "loop:" or "branch:" accordingly. Moreover, <= and >= are normalized to < and >
   * An example representation string may look like "loop:LT loop:LT branch:EQ". See {@link Representations#relationalOperators}
   * for information on which strings correspond to which operator
   * @param code
   * @return string of space separated tokens
   */
  public static String getSmarterRelationalOperators(String code) {
    StringBuilder sb = new StringBuilder();
    CtClass l = Launcher.parseClass(code);
    List<CtBinaryOperator> operators = l.getElements(new TypeFilter<>(CtBinaryOperator.class));

    List<CtExpression> loops = new ArrayList<>();

    // START LOOPS
    List<CtFor> forLoops = l.getElements(new TypeFilter<>(CtFor.class));
    for(CtFor forLoop : forLoops) if (forLoop.getExpression() != null) loops.add(forLoop.getExpression());

    List<CtForEach> forEachLoops = l.getElements(new TypeFilter<>(CtForEach.class));
    for(CtForEach forEach : forEachLoops) loops.add(forEach.getExpression());

    List<CtDo> doLoops = l.getElements(new TypeFilter<>(CtDo.class));
    for (CtDo doLoop : doLoops) loops.add(doLoop.getLoopingExpression());

    List<CtWhile> whileLoops = l.getElements(new TypeFilter<>(CtWhile.class));
    for(CtWhile loop : whileLoops) loops.add(loop.getLoopingExpression());

    // END LOOPS

    for (CtExpression loop : loops) {
      List<CtBinaryOperator> conditionalTypes = loop.getElements(new TypeFilter<>(CtBinaryOperator.class));
      for (CtBinaryOperator operator : conditionalTypes) {
        sb.append("loop:" + operator.getKind().toString() + String.valueOf(operator.getType()));
        sb.append(" ");
      }
    }

    List<CtExpression> conditionals = new ArrayList<>();
    // START CONDITIONALS

    List<CtIf> ifs = l.getElements(new TypeFilter<>(CtIf.class));
    for(CtIf branch : ifs) conditionals.add(branch.getCondition());

    List<CtSwitch> switches = l.getElements(new TypeFilter<>(CtSwitch.class));
    for(CtSwitch branch : switches) conditionals.add(branch.getSelector());

    // ternary operator
    List<CtConditional> ternaryOperators = l.getElements(new TypeFilter<>(CtConditional.class));
    for(CtConditional ternary : ternaryOperators) conditionals.add(ternary.getCondition());


    for (CtExpression conditional : conditionals) {
      List<CtBinaryOperator> conditionalTypes = conditional.getElements(new TypeFilter<>(CtBinaryOperator.class));
      for (CtBinaryOperator operator : conditionalTypes) {
        sb.append("branch:" + operator.getKind().toString() + String.valueOf(operator.getType()));
        sb.append(" ");
      }
    }

    //TODO: some topics benefit from normalization, others don't. Find a better way
    return sb.toString().trim()
        .replaceAll("LE", "LT")
        .replaceAll("GE", "GT");
  }

  /**
   * This representations lists all relational operators in code, same number of times as they occur in the code.
   * Moreover, <= and >= are normalized to < and >.
   * An example representation string may look like "LT LT EQ". See {@link Representations#relationalOperators}
   * for information on which strings correspond to which operator.
   * @see Representations#getSmarterRelationalOperators(String)
   * @param code
   * @return string of space separated tokens
   */
  public static String getRelationalOperators(String code) {
    StringBuilder sb = new StringBuilder();
    CtClass l = Launcher.parseClass(code);
    List<CtBinaryOperator> operators = l.getElements(new TypeFilter<>(CtBinaryOperator.class));

    for(CtBinaryOperator operator : operators) {
      if (relationalOperators.contains(operator.getKind().name())) {
        sb.append(" ");
        sb.append(operator.getKind().toString() + String.valueOf(operator.getType()));
      }
    }

    //TODO: some topics benefit from normalization, others don't. Find a better way
    return sb.toString().trim()
        .replaceAll("LE", "LT")
        .replaceAll("GE", "GT");
  }

  /**
   * This representation lists method names and operators which perform such function (e.g. ++) which are found in the
   * cdoe, with same frequency as they're found in code
   * This considers unary operators (e.g. -), binary operators (+, -, *, ...) and any functions which are called.
   * @param code
   * @return string of space separated tokens
   */
  public static String getMethodsAndFunctionalOperators(String code) {
    StringBuilder sb = new StringBuilder();
    CtClass l = Launcher.parseClass(code);
    List<CtInvocation> invocations = l.getElements(new TypeFilter<>(CtInvocation.class));

    for(CtInvocation invocation : invocations) {
      sb.append(" ");
      sb.append(invocation.getExecutable().toString());
    }

    List<CtBinaryOperator> operators = l.getElements(new TypeFilter<>(CtBinaryOperator.class));

    for(CtBinaryOperator operator : operators) {
      if (!relationalOperators.contains(operator.getKind().name())) {
        sb.append(" ");
        sb.append(operator.getKind().toString() + String.valueOf(operator.getType()));
      }
    }

    List<CtUnaryOperator> unaryOperators = l.getElements(new TypeFilter<>(CtUnaryOperator.class));

    for(CtUnaryOperator operator : unaryOperators) {
      sb.append(" ");
      sb.append(operator.getKind().toString() + String.valueOf(operator.getType()));
    }

    return sb.toString().trim();
  }

  /**
   * This representation uses all types which may appear anywhere in the AST, as long as they're within a loop or a branch.
   * It prefixes them with "loop:" or "branch:" accordingly.
   * @param code
   * @return string of space separated tokens
   */
  public static String getTypes(String code) {
    StringBuilder sb = new StringBuilder();
    CtClass l = Launcher.parseClass(code);
    List<CtAssignment> assignmentList = l.getElements(new TypeFilter(CtAssignment.class));

    List<CtExpression> loops = new ArrayList<>();

    // START LOOPS
    List<CtFor> forLoops = l.getElements(new TypeFilter<>(CtFor.class));
    for(CtFor forLoop : forLoops) if (forLoop.getExpression() != null) loops.add(forLoop.getExpression());

    List<CtForEach> forEachLoops = l.getElements(new TypeFilter<>(CtForEach.class));
    for(CtForEach forEach : forEachLoops) loops.add(forEach.getExpression());

    List<CtDo> doLoops = l.getElements(new TypeFilter<>(CtDo.class));
    for (CtDo doLoop : doLoops) loops.add(doLoop.getLoopingExpression());

    List<CtWhile> whileLoops = l.getElements(new TypeFilter<>(CtWhile.class));
    for(CtWhile loop : whileLoops) loops.add(loop.getLoopingExpression());

    for (CtExpression loop : loops) {
      List<CtTypeReference> conditionalTypes = loop.getElements(new TypeFilter<>(CtTypeReference.class));
      for (CtTypeReference conditionalType : conditionalTypes) {
        sb.append("loop:" + conditionalType);
        sb.append(" ");
      }
    }

    List<CtExpression> conditionals = new ArrayList<>();
    // START CONDITIONALS

    List<CtIf> ifs = l.getElements(new TypeFilter<>(CtIf.class));
    for(CtIf branch : ifs) conditionals.add(branch.getCondition());

    List<CtSwitch> switches = l.getElements(new TypeFilter<>(CtSwitch.class));
    for(CtSwitch branch : switches) conditionals.add(branch.getSelector());

    // ternary operator
    List<CtConditional> ternaryOperators = l.getElements(new TypeFilter<>(CtConditional.class));
    for(CtConditional ternary : ternaryOperators) conditionals.add(ternary.getCondition());


    for (CtExpression conditional : conditionals) {
      List<CtTypeReference> conditionalTypes = conditional.getElements(new TypeFilter<>(CtTypeReference.class));
      for (CtTypeReference conditionalType : conditionalTypes) {
        sb.append("branch:" + conditionalType);
        sb.append(" ");
      }
    }

    String rep = sb.toString().trim();
    return rep;
  }

  /**
   * This is just a wrapper. See CFGALLPaths class.
   * @see CFGAllPaths
   * @param code
   * @param fileName
   * @return string of space separated tokens
   * @throws IOException
   */
  public static String getAllPathsRepresentation(String code, String fileName) throws IOException {
    String rep = CFGAllPaths.getAllPathsRepresentation(code, fileName);
    return rep;
  }

  /**
   * Overloaded method providing a default file name
   * @see Representations#getAllPathsRepresentation(String, String)
   * @param code
   * @return string of space separated tokens
   * @throws IOException
   */
  public static String getAllPathsRepresentation(String code) throws IOException {
    return getAllPathsRepresentation(code, "Test.java");
  }
}
