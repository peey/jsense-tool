package in.edu.iiitd.pag.jsense.representations;

import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.JavacTask;
import org.checkerframework.dataflow.cfg.CFGBuilder;
import org.checkerframework.dataflow.cfg.ControlFlowGraph;
import org.checkerframework.dataflow.cfg.block.*;

import javax.tools.*;

/**
 * Extracts flows and represents them as strings.
 * @author Peeyush Kushwaha
 */
class CFGAllPaths {
  public static String rep(Block b) {
    switch(b.getType()) {
      case REGULAR_BLOCK: return ((RegularBlock) b).toString();
      case EXCEPTION_BLOCK: return ((ExceptionBlock) b).getNode().toString();
      case SPECIAL_BLOCK: return b.toString();
      case CONDITIONAL_BLOCK: return ((ConditionalBlock) b).getThenFlowRule().toString();
      default: System.out.printf("Warning: Unhandled case of block of type %s \n", String.valueOf(b.getType()));
    }

    return "See warning";
  }

  public static void main(String[] args) throws Exception {
    String code = "class Hello { public static void main(String[] args) { " +
        "boolean condition = false; Integer i = 3; Integer j = i + 1; " +
        "while (condition) { " +
        "i = j + 10; " +
        "System.out.println(\"Hello World\"); " +
        "} " +
        "System.out.println(\"Goodbye, world\"); " +
        "if (condition) {System.out.println(\"Hello Conditional \");} else {System.out.println(\"Goodbye, conditional\");}" +
        "}}";

    String apr = getAllPathsRepresentation(code, "Hello.java");
    System.out.println(apr);
  }

  /**
   * A complicated wrapper to programmatically access javac and be able to use checker framework with it
   * @param sfo
   * @return CompilationUnitTree object which the {@link CFGAllPaths#getCFG(CompilationUnitTree)} method expects
   * @throws IOException
   */
  public static CompilationUnitTree getCUnitTree(JavaFileObject sfo) throws IOException {

    java.util.List<JavaFileObject> files = Arrays.asList(sfo);
    JavaCompiler tool = ToolProvider.getSystemJavaCompiler();

    JavacTask ct = (JavacTask)tool.getTask(null, null, null, Arrays.asList( "-cp", "./framework/checker/dist/checker.jar", "-Xbootclasspath/p:\"./framework/checker/dist/jdk8.jar\""), null, files);

    ct.setProcessors(Arrays.asList(new AlmostEmptyProcessor()));

    Iterable<? extends CompilationUnitTree> compUnits = ct.parse();
    // analysis happens after parsing. Assuming any effects of processing affect the tree in-place so we don't need to update our reference to compilation units
    ct.analyze();

    CompilationUnitTree cut = compUnits.iterator().next();
    return cut;
  }

  /**
   * Gets control flow graph. You need to make a CompilationUnitTree first using {@link CFGAllPaths#getCUnitTree(JavaFileObject)}
   * @param cut
   * @return the control flow graph
   */
  public static ControlFlowGraph getCFG(CompilationUnitTree cut) {
    ClassTree clazz = (ClassTree) cut.getTypeDecls().get(0);

    List<? extends Tree> clazzMembers = clazz.getMembers();
    List<MethodTree> methodTrees = (List<MethodTree>) clazzMembers.stream().filter(x -> x instanceof MethodTree).collect(Collectors.toList());
    // zeroth method is the implicit constructor
    MethodTree mt = (MethodTree) methodTrees.get(1);
    if (methodTrees.size() > 2) System.out.println("Method tree size is actually " + methodTrees.size());

    return CFGBuilder.build(cut, mt, clazz, AlmostEmptyProcessor.processingEnv);
  }

  /**
   * The javac compiler can't be given the source code directly as a string, it expects it in a JavaFileObject.
   * So we create a virtual JavaFileObject using the contents of the snippet.
   * @param code
   * @param fileName
   * @return file object expected by {@link CFGAllPaths#getCUnitTree(JavaFileObject)}
   */
  public static JavaFileObject fileObjectFromCode(String code, String fileName) {
    return new SimpleJavaFileObject(URI.create("myfo:/" + fileName), JavaFileObject.Kind.SOURCE) {
      public CharSequence getCharContent(boolean ignoreEncodingErrors) {
        return code;
      }
    };
  }

  /**
   * @see CFGAllPaths#fileObjectFromCode(String, String)
   * @param code
   * @return file object expected by {@link CFGAllPaths#getCUnitTree(JavaFileObject)}
   */
  public static JavaFileObject fileObjectFromCode(String code) {
    return fileObjectFromCode(code, "Test.java");
  }

  /**
   * Gets successors of a CFG node b
   * @param cfg the CFG instance
   * @param b the node
   * @return set of successors
   */
  public static Set<Block> successors(ControlFlowGraph cfg, Block b) {
    Set<Block> neighbors  = new HashSet<>();
    switch(b.getType()) {
      case REGULAR_BLOCK:
        neighbors.add(((RegularBlock) b).getRegularSuccessor()); break;
      case EXCEPTION_BLOCK: neighbors.add(((ExceptionBlock) b).getSuccessor()); break;
      case SPECIAL_BLOCK:
        if (((SpecialBlock) b).getSpecialType() == SpecialBlock.SpecialBlockType.ENTRY) {
          neighbors.add(((SpecialBlock) b).getSuccessor());
          // other types are exit and exceptional exit
        }
        break;
      case CONDITIONAL_BLOCK:
        neighbors.add(((ConditionalBlock) b).getThenSuccessor());
        neighbors.add(((ConditionalBlock) b).getElseSuccessor());
        break;
      default: System.out.printf("Warning: Unhandled case of block of type %s \n", String.valueOf(b.getType()));
    }

    // remove null entries, sometimes the successor is null
    return neighbors.stream().filter(x -> x != null).collect(Collectors.toSet());
  }

  /**
   * Used for debugging
   */
  public static void printWithIndent(Object toPrint, int indent) {
    /*
    while(indent-- > 0) {
      System.out.print("  ");
    }
    System.out.println(toPrint);
    */
  }


  /**
   * If you pass it a conditional block then it will return the block itself
   * @param b
   */
  public static ConditionalBlock firstConditionalSuccessor(Block b) {
    switch (b.getType()) {
      case CONDITIONAL_BLOCK: return (ConditionalBlock) b;
      case REGULAR_BLOCK: return firstConditionalSuccessor(((RegularBlock) b).getRegularSuccessor());
      case EXCEPTION_BLOCK: return firstConditionalSuccessor(((ExceptionBlock) b ).getSuccessor());
      case SPECIAL_BLOCK:
        if (((SpecialBlock) b).getSpecialType() == SpecialBlock.SpecialBlockType.ENTRY) {
          return firstConditionalSuccessor(((SpecialBlock) b).getSuccessor());
        } else {
          return null;
        }
    }

    return null; // unnecessary but my ide is dumb
  }

  //returns exit node

  /**
   * Recursive dfs over the CFG to identify which nodes are branch entry and exit nodes and which nodes are loop entry
   * and exit nodes.
   * @param cfg the CFG instance
   * @param b block being currently visited
   * @param branchEntry a set to which we'll add all the blocks which we identify as branch entries
   * @param branchExit  a set to which we'll add all the blocks which we identify as branch exits
   * @param loopEntry   a set to which we'll add all the blocks which we identify as loop entries
   * @param loopExit    a set to which we'll add all the blocks which we identify as loop exits
   * @param visited  internally used to keep track of visited blocks. The caller must initialize with an empty set
   * @param currentPath internally used to keep track of current nodes on the stack in the DFS
   */
  public static void annotation_dfs_visit(ControlFlowGraph cfg, Block b, Set<ConditionalBlock> branchEntry, Set<Block> branchExit, Set<ConditionalBlock> loopEntry, Set<Block> loopExit, Set<Block> visited, Stack currentPath) {
    currentPath.push(b);
    visited.add(b);


    // logic before backtrack
    /*
    System.out.println(b.getType());
    System.out.println(b.toString());
    System.out.println(rep(b));
    */
    switch(b.getType()) {
      case CONDITIONAL_BLOCK: branchEntry.add((ConditionalBlock) b);
    }
    //end logic

    Set<Block> neighbors = successors(cfg, b);

    Set<Block> unvisitedNeighbors = neighbors.stream().filter(x -> !visited.contains(x)).collect(Collectors.toSet());
    Set<Block> visitedNeighbors = neighbors.stream().filter(visited::contains).collect(Collectors.toSet());

    for (Block vn : visitedNeighbors) {
      if (currentPath.contains(vn)) {
        // this means it's a backedge to loop
        loopEntry.add(firstConditionalSuccessor(vn));
        loopExit.add(b);
      } else {
        //otherwise it's a meet point for branch
        branchExit.add(vn);
      }
    }

    if(neighbors.isEmpty()) {
      // must be exit block
      printWithIndent("leaf", currentPath.size());
    }

    printWithIndent("visiting children for " + b.toString(), currentPath.size());
    for(Block uvn: unvisitedNeighbors) {
      if (b instanceof ConditionalBlock) {
        boolean visitedsadly = visited.contains(uvn);
        printWithIndent(visitedsadly, currentPath.size());
        printWithIndent(unvisitedNeighbors.size(), currentPath.size());
      }
      printWithIndent("child " + uvn.toString(), currentPath.size());
      annotation_dfs_visit(cfg, uvn, branchEntry, branchExit, loopEntry, loopExit, visited, currentPath);
    }
    // backtracking
    currentPath.pop();
  }

  /**
   * Reasons if a an edge is a loop back edge
   * @param src source node
   * @param dstn destination node
   * @param loopEntry calculated loop entry nodes
   * @param loopExit  calculated loop exit nodes
   * @return
   */
  public static boolean isLoopBackEdge(Block src, Block dstn, Set<ConditionalBlock> loopEntry, Set<Block> loopExit) {
    //TODO handle other kinds than regular block
    if (loopExit.contains(src) && dstn instanceof RegularBlock && ((RegularBlock) dstn).getRegularSuccessor() instanceof ConditionalBlock) {
      // if the destination is a loop entry's head (it's successor is a loop entry) then yes, we have a back edge
      return loopEntry.contains((ConditionalBlock) ((RegularBlock) dstn).getRegularSuccessor());
    } else {
      return false;
    }
  }

  /**
   * Loop heads are actually regular blocks whose successors are conditional blocks
   * @param b
   * @return true if a node is a loop head
   */
  public static boolean isRegularHavingConditional(Block b) {
    return b instanceof RegularBlock && ((RegularBlock) b).getRegularSuccessor() instanceof ConditionalBlock;
  }

  /**
   * Gets flows from a CFG. Requires information about calculated loop entry and loop exits which can be obtained
   * by calling {@link CFGAllPaths#annotation_dfs_visit(ControlFlowGraph, Block, Set, Set, Set, Set, Set, Stack)}
   * For every branch we add a new flow, and for every loop we consider two branches - one which loops 0 times and one
   * which loops once. This allows us to capture the contents of the loop body precisely once. This is how we're able
   * to get "all flows" despite there being infinte possible flows due to loops
   * @param cfg the CFG instance
   * @param initialB current DFS node
   * @param visited internal set of visited nodes
   * @param loopVisited internal set of loop nodes which have been visited so that we process each loop only once
   * @param currentPath current nodes on DFS stack
   * @param cache contains flows starting at a particular node for dynamic programming
   * @param loopEntry computed information about nodes which are loop entry nodes
   * @param loopExit  computed information about nodes which are loop exit nodes
   * @return A list of flows
   */
  public static List<List<Block>> getAllFlows_dfs(ControlFlowGraph cfg, Block initialB, Set<Block> visited, Set<ConditionalBlock> loopVisited, Stack<Block> currentPath, Map<Block, List<List<Block>>> cache, Set<ConditionalBlock> loopEntry, Set<Block> loopExit) {
    currentPath.push(initialB);
    List<List<Block>> allFlows = new ArrayList<>();

    // usually current node, but in case of a loop it contains more
    List<Block> fromCurrentNode = new ArrayList<>();

    boolean loopTime = false;

    if (visited.contains(initialB)) {
      if (cache.containsKey(initialB)) {
        // base case 1 - visited
        currentPath.pop();
        return cache.get(initialB);
      } else {
        // we are in a loop, yay!
        assert isRegularHavingConditional(initialB) : "Invalid state, we should be in a regular having a conditional";

        Stack<Block> cpClone = (Stack<Block>) currentPath.clone();

        cpClone.pop();
        Stack<Block> toReverse = new Stack<>();
        while(cpClone.peek() != initialB) {
          toReverse.push(cpClone.pop());
        }

        while(!toReverse.isEmpty()) {
          fromCurrentNode.add(toReverse.pop());
        }

        loopVisited.add(((ConditionalBlock) ((RegularBlock) initialB).getSuccessor()));
        loopTime = true;

        /*
        try {
        } catch (Exception e) {
          throw new IllegalStateException("We should only be here when coming back from a loop");
        }
        */
      }
    }

    final Block b = loopTime? ((RegularBlock) initialB).getRegularSuccessor() : initialB;

    if (fromCurrentNode.isEmpty()) {
      fromCurrentNode.add(b);
    }


    visited.add(b);

    // do stuff

    Set<Block> successorSet = successors(cfg, b);
    // prevent from considering loop bodies more than once
    Set<Block> visitableSuccessors = successorSet.stream().filter(x -> isLoopBackEdge(b, x, loopEntry, loopExit) && !loopVisited.contains(x) || !currentPath.contains(x)).collect(Collectors.toSet());
    //Set<Block> unvisited = successorSet.stream().filter(x -> !visited.contains(x)).collect(Collectors.toSet());

    if (successorSet.isEmpty()) {
      // base case 2 - exit node
      allFlows.add(Arrays.asList(b));
    } else {
      for (Block successor : visitableSuccessors) {
        /*
        if (isLoopBackEdge(b, successor, loopEntry, loopExit)) {
          continue; // don't pursue a loop backedge
        }
        */
        List<List<Block>> successorFlows = getAllFlows_dfs(cfg, successor, visited, loopVisited, currentPath, cache, loopEntry, loopExit);
        for (List<Block> sFlow : successorFlows) {
          List<Block> freshTypeOfFlow = new ArrayList<>();
          freshTypeOfFlow.addAll(fromCurrentNode);
          freshTypeOfFlow.addAll(sFlow);
          allFlows.add(freshTypeOfFlow);
        }
      }
    }

    currentPath.pop();
    cache.put(b, allFlows);
    return allFlows;
  }

  /**
   * end-to-end function which given code and a filename, gives you the string representation of all paths. Internally
   * uses all the other methods in this class.
   * TODO/ENHANCEMENT: To convert a flow to a string, currently we only consider structural information of a flow - if a
   * node is starting of a branch, ending of a branch, starting of a loop, ending of a loop. We can experiment with
   * other information like types.
   * @param code
   * @param fileName
   * @return string representation of all paths.
   * @throws IOException
   */
  public static String getAllPathsRepresentation(String code, String fileName) throws IOException {
    ControlFlowGraph cfg = getCFG(getCUnitTree(fileObjectFromCode(code, fileName)));

    //regular DFS and annotate

    Set<ConditionalBlock> branchEntry = new HashSet();
    Set<Block> branchExit = new HashSet();
    Set<ConditionalBlock> loopEntry = new HashSet();
    Set<Block> loopExit = new HashSet();

    annotation_dfs_visit(cfg, cfg.getEntryBlock(),branchEntry, branchExit, loopEntry, loopExit, new HashSet(), new Stack());

    //branch entries except loops
    Set<ConditionalBlock> branchEntriesExceptLoops = branchEntry.stream().filter(x -> !loopEntry.contains(x)).collect(Collectors.toSet());

    //List<List<Block>> allFlows = getAllFlows(cfg, loopEntry, loopExit);
    List<List<Block>> allFlows = getAllFlows_dfs(cfg, cfg.getEntryBlock(), new HashSet<>(),  new HashSet<>() , new Stack<>(), new HashMap<>(), loopEntry, loopExit);

    StringBuilder sb = new StringBuilder();
    for(List<Block> flow : allFlows) {
      for (Block b: flow) {
        switch (b.getType()) {
          case CONDITIONAL_BLOCK:
            if(branchEntriesExceptLoops.contains(b)) {
              sb.append("<branch>");
            } else if(loopEntry.contains(b)) {
              sb.append("<loop>");
            }
        }

        if (branchExit.contains(b)) {
          sb.append("</branch>");
        }
        //TODO: elseif?
        if (loopExit.contains(b)) {
          sb.append("</loop>");
        }
      }

      sb.append(" ");
    }

    // Now, after annotated CFG, we go for all paths. We won't consider the back edges so it's really a DAG

    return sb.toString();
  }
}
