package in.edu.iiitd.pag.jsense;

import in.edu.iiitd.pag.jsense.vss.VectorSpace;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.math.BigInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static in.edu.iiitd.pag.jsense.API.API.debugOutput;

/**
 * Contains helper methods / utils
 * @author Peeyush Kushwaha
 */
public class Helpers {

  /**
   * Calculates f1 score given precision and recall values
   * @param precision
   * @param recall
   */
  public static Double fScore(Double precision, Double recall) {
    return 2 * (precision*recall) / (precision + recall);
  }

  static Pattern wordPattern = Pattern.compile("[\\S]+");

  /**
   * Utility function used by many representations. Considers each word (separated by whitespace) in input to be a separate
   * term. Quite useful if you're reducing code to a string representation.
   * @param s
   * @param vecspace
   * @return
   */
  public static VectorSpace.Vector makeVectorTokenizingByWhitespace(String s, VectorSpace vecspace) {
    VectorSpace.Vector v = vecspace.new Vector();

    for (Matcher m = wordPattern.matcher(s); m.find(); ) {
      String word = s.substring(m.start(), m.end());
      v.set(word, v.get(word) + 1);
    }

    return v;
  }

  /**
   * Default pattern for snippet identifiers is topic<id>.
   * @param topic
   * @param id
   * @return identifier, given the topic and id
   */
  public static String snippetIdentifier(String topic, Integer id) {
    return topic + "<" + id + ">";
  }

  // use for supressing output https://stackoverflow.com/a/8363580
  // but also store output so that it can be flushed in case of an error https://stackoverflow.com/a/1760668/
  final static ByteArrayOutputStream baosOut = new ByteArrayOutputStream();
  final static ByteArrayOutputStream baosErr = new ByteArrayOutputStream();
  static PrintStream dummyOut;
  static PrintStream dummyErr;

  static {
    try {
      dummyOut = new PrintStream(baosOut, true, "UTF-8");
      dummyErr = new PrintStream(baosErr, true, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
  }

  final static PrintStream originalStream = System.out;
  final static PrintStream originalErrStream = System.err;

  /**
   * Supresses anything written to System.out till the corresponding resumeOP function is called.
   * Mostly useful when using external libraries like checker framework which put compilation information on standard
   * output when dealing with snippets.
   * @see Helpers#resumeOP()
   */
  public static void supressOP() {
    if (debugOutput) System.out.println("Supressing output");
    System.setOut(dummyOut);
    System.setErr(dummyErr);
  }

  /**
   * Supresses anything written to System.out till the corresponding resumeOP function is called.
   * Mostly useful when using external libraries like checker framework which put compilation information on standard
   * output when dealing with snippets.
   * @see Helpers#supressOP() ()
   */
  public static void resumeOP() {
    System.setOut(originalStream);
    System.setErr(originalErrStream);
    if(debugOutput) System.out.println("Resumed output");
  }

  /**
   * Prints the stored supressed output to the output stream
   * @see Helpers#supressOP()
   * @see Helpers#resumeOP()
   */
  public static void fulshSupressedOP() {
    //note that interleaved err and out messages would now appear one after another. There should be a better way to do
    //this
    System.err.println(new String(baosErr.toByteArray(), StandardCharsets.UTF_8));
    System.out.println(new String(baosOut.toByteArray(), StandardCharsets.UTF_8));
  }

  /**
   * get bit from bit representation of the integer at the given position
   * from https://stackoverflow.com/a/1092551/1412255
   */
  public static boolean getBit(int value, int position) {
    return BigInteger.valueOf(value).testBit(position);
  }

  /**
   * Replaces some types with a canonical word. e.g. float and double are made 'real'
   * Please see the function source to see the normalizations performed.
   * @param input
   * @return normalized output
   */
  public static String normalizeTypes(String input) {
    return input
        .replaceAll("(int|short|long)", "integral")
        .replaceAll("(float|double)", "real")
        .replaceAll("(Integer|Short|Long)", "Integral")
        .replaceAll("(Float|Double)", "Real")
        .replaceAll(" (\\S*)(integral|real|Integral|Real)(\\S*) ", " $1$2$3 $1numeric$3"); // replaces words with integral|real|... as prefixes
  }
}
