
package in.edu.iiitd.pag.jsense.vss;

import in.edu.iiitd.pag.jsense.Helpers;
import in.edu.iiitd.pag.jsense.representations.Representations;
import in.edu.iiitd.pag.jsense.snippets.Snippet;

import java.io.IOException;

/**
 * The classes ending in DocumentVectorSingleton describe different ways of representing a code snippet as a document.
 * This is just a wrapper. The main logic is in Representations.java
 * @author Peeyush Kushwaha
 */
public class RelationalOperatorVectorSingleton extends DocumentVectorsSingleton {
  public static final DocumentVectorsSingleton singleton = new RelationalOperatorVectorSingleton();

  final VectorSpace<String> relationalVecspace = new VectorSpace<String>();

  private RelationalOperatorVectorSingleton() {};

  public VectorSpace.Vector makeVector(Snippet s) throws IOException {
    String code = s.getWrappedCode();

    //String rep = Representations.getRelationalOperators(code);

    String rep = Representations.getSmarterRelationalOperators(code);

    return Helpers.makeVectorTokenizingByWhitespace(rep, relationalVecspace);
  }
}
