/**
 * The purpose of the VectorSpace module is to implement the data structure for a vector with an associated Vector Space
 * A Vector Space has dimensions.
 * Right now, a vector space is just used for dimensions. A vector spaces IS NOT a collection of vectors, that's
 * Collection<VectorSpace.Vector>. A vector space is just a space - like a 3d space, having just the information of the
 * dimensions.
 */

package in.edu.iiitd.pag.jsense.vss;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import java.util.*;

/**
 * Implements sparse vectors, saves space by maintaining a big list of vectorDimensions as new ones are discovered
 * and then mapping them to integers. Thus all keys that this class internally deals with are integers. Ways to convert
 * a dimension to corresponding integer key and vice versa are provided
 * @author Peeyush Kushwaha
 */
public class VectorSpace<T> {

  // Dimensions are mapped to natural numbers in the order they are seen. Mapping is stored in vectorDimensions
  private int maxAssignedIndex = 0;
  // We use a bidirectional map so that we can efficiently look up our assigned integer keys from the provided real keys
  // and vice versa
  private HashBiMap<T, Integer> vectorDimensions = HashBiMap.create();

  /**
   * Vector class is implemented as a inner class since all vectors in a vector space will share the same dimensions.
   * And the list of dimensions is maintained by the outer class.
   * @author Peeyush Kushwaha
   */
  public class Vector implements Cloneable {
    private HashMap<Integer, Double> dimensions = new HashMap<>();

    /**
     * Any actions involving two vectors should make sure that the vectors belong to the same vector space. This variable
     * is initialized on creation of the vector. Anything that requires access to dimensions should check this. We use
     * this in our code for equality checking and {@link Vector#dot(Vector)}
     */
    private final VectorSpace vectorSpace;

    /**
     * Get the component of the vector for a specified dimension
     * @param entry the dimension
     * @return vector component along the specified dimension
     */
    public double get(T entry) {
      if (!dimensions.containsKey(vectorDimensions.get(entry))) {
        return 0;
      } else {
        return dimensions.get(vectorDimensions.get(entry));
      }
    }

    /**
     * Set the component of the vector for a specified dimension
     * @param entry the dimension
     * @param value value for the component
     */
    public void set(T entry, Double value) {
      if (!vectorDimensions.containsKey(entry)) {
        vectorDimensions.put(entry, maxAssignedIndex++);
      }

      dimensions.put(vectorDimensions.get(entry), value);
    }

    /**
     * @see Vector#set(Object, Double)
     * @param entry the dimension
     * @param value value for the component
     */
    public void set(T entry, Integer value) {
      this.set(entry, (double) value);
    }

    public Double norm2() {
      Double result = .0;
      for (Integer key: dimensions.keySet()) { // dimensions not present on this vector contribute 0 anyways
        result += Math.pow(dimensions.get(key), 2);
      }
      return Math.sqrt(result);
    }

    public Vector(HashMap<Integer, Double> dimensions) {// allow initialization by a hashmap, useful for making clones
      this.dimensions = dimensions;
      vectorSpace = VectorSpace.this;
    }

    /**
     * Create a new vector with all components 0
     */
    public Vector() {vectorSpace = VectorSpace.this;}

    /**
     * Creates a new vector with scaled vector components so that norm2() is 1. Doesn't mutate the original vector.
     * @return a normalized vector
     */
    public Vector normalize() {
      HashMap<Integer, Double> normalizedHashMap = (HashMap<Integer, Double>) dimensions.clone();
      Double l = norm2();
      for(Integer k: dimensions.keySet()) {
        normalizedHashMap.put(k, dimensions.get(k)/l);
      }
      return new Vector(normalizedHashMap);
    }

    /**
     * Calculates scalar/dot product between this vector and the provided vector
     * @param v the vector to calculate dot product with
     * @return dot product
     */
    public Double dot(Vector v) {
      assert vectorSpace == v.vectorSpace : "Scoring between two vectors from different vector spaces should not be performed";

      Double result = .0;
      for (Integer k: this.dimensions.keySet()) {
        if (v.dimensions.containsKey(k)) {
          result += this.dimensions.get(k) * v.dimensions.get(k);
        }
      }
      return result;
    }

    public Vector add(Vector v) {
      Vector result = this.clone();

      for(Integer key : v.dimensions.keySet()) {
        Double val = result.dimensions.getOrDefault(key, .0) + v.dimensions.get(key);
        result.dimensions.put(key, val);
      }

      return result;
    }

    /**
     * Useful for finding if we have a zero vector.
     * @param tolerance
     * @return true if norm2() is less than tolerance
     */
    public boolean isEmpty(Double tolerance) {
      return this.norm2() <= tolerance;
    }

    /**
     * Returns a new vector with entries scaled to provided weights. Creates a new vector, does not mutate this vector.
     * @param weights for every dimension, a weight value
     * @return the scaled vector.
     */
    public Vector weighted(Map<T, Double> weights) {
      Vector result = new Vector();

      for (T key: (HashSet<T>) entries()) {
        Double w = weights.get(key);
        result.set(key, w*this.get(key));
      }

      return result;
    }

    /**
     * Returns the dimensions which were set for the vector (could be 0)
     * @return set of dimensions
     */
    public HashSet<T> entries() {
      HashSet<T> result = new HashSet<T>();
      BiMap<Integer, T> inv = vectorDimensions.inverse();
      for (Integer k : this.dimensions.keySet()) {
        result.add((T) inv.get(k));
      }
      return result;
    }

    /**
     * @see Vector#toString(String)
     * @return string representation with entries separated by a single space
     */
    @Override
    public String toString() {
      return toString(" ");
    }

    /**
     * @return string representation with entries separated by specified delimiter
     */
    public String toString(String delimiter)  {

      StringBuilder sb = new StringBuilder();

      sb.append("Vector[");
      T[] keyArray = (T[]) (entries()).toArray();
      Arrays.sort(keyArray);

      for(T key: keyArray) {
        sb.append(delimiter + "(" + key.toString() + ", " + get(key) + ")");
      }
      sb.append(" ]");

      return sb.toString();
    }

    /**
     * Checks if the given vector's components are same as this vector's along all dimensions, with the given tolerance
     * @param v given vector to compare equality with
     * @param tolerance
     * @return true if equal
     */
    public boolean equals(Vector v, Double tolerance) {
      assert vectorSpace == v.vectorSpace : "Comparison between two vectors from different vector spaces should not be performed";

      if (!this.dimensions.keySet().equals(v.dimensions.keySet())) {
        return false;
      } else {
        for (Integer key : this.dimensions.keySet()) {
          if (Math.abs(this.dimensions.get(key) - v.dimensions.get(key)) > tolerance) {
            System.out.println(Math.abs(this.dimensions.get(key) - v.dimensions.get(key)));
            return false;
          }
        }

        return true;
      }
    }

    /**
     * Checks equality with a default tolerance of 0.001
     * @see {@link Vector#equals(Vector, Double)}
     * @param v given vector to compare equality with
     * @return true if equal
     */
    public boolean equals(Vector v) {
      return equals(v, 0.001);
    }

    /**
     * @return a clone of current vector with same components along each dimension
     */
    public Vector clone() {
      return new Vector((HashMap) this.dimensions.clone());
    }
  }
}

