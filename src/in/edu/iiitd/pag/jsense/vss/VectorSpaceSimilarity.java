package in.edu.iiitd.pag.jsense.vss;

import in.edu.iiitd.pag.jsense.snippets.Snippet;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * {@link SimilarityStrategy} implementation for cosine similarity using vector space model
 * @author Peeyush Kushwaha
 */
public class VectorSpaceSimilarity extends SimilarityStrategy {
  //TODO convert all of this to Map<Snippet, Vector>
  // cache of created vector for each document
  public Map<String, VectorSpace.Vector> jdata = new HashMap<>();
  public Map<String, Double> idf = new HashMap<>();
  // we need an instance of dvSingleton to get IDF and document frequency
  public DocumentVectorsSingleton dvSingleton;
  //cache of normalized vector
  Map<String, VectorSpace.Vector> normalizedJdataVectors = new HashMap<>();

  public VectorSpaceSimilarity(Map<Snippet, VectorSpace.Vector> dv, DocumentVectorsSingleton dvSingleton) {
    super(dv.keySet());

    this.dvSingleton = dvSingleton;

    jdata = dv.keySet().stream().collect(Collectors.toMap((Snippet s) -> s.identifier, (Snippet s) -> dv.get(s)));
    idf = dvSingleton.getIDF(dvSingleton.getDocumentFrequency(jdata.values()), jdata.size());

    //Normalize and cache
    for (String file : jdata.keySet()) {
      normalizedJdataVectors.put(file, jdata.get(file).weighted(idf).normalize());
    }
  }

  /**
   * Gets cosine similarity between the vectors of the two documents
   * @param file1 identifier for document 1
   * @param file2 identifier for document 2
   * @return cosine similarity score
   */
  public Double score(String file1, String file2) {
    Double result = normalizedJdataVectors.get(file1).dot(normalizedJdataVectors.get(file2));
    return result;
  }
}
