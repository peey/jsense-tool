package in.edu.iiitd.pag.jsense.vss;

import in.edu.iiitd.pag.jsense.Helpers;
import in.edu.iiitd.pag.jsense.snippets.Snippet;

import java.io.IOException;

/**
 * The classes ending in DocumentVectorSingleton describe different ways of representing a code snippet as a document.
 * This is just a wrapper. The main logic is in Representations.java
 * @author Peeyush Kushwaha
 */
public class BaselineDocumentVectorsSingleton extends DocumentVectorsSingleton {
  public static final DocumentVectorsSingleton singleton = new BaselineDocumentVectorsSingleton();

  final VectorSpace<String> baselineVecspace = new VectorSpace<String>();

  private BaselineDocumentVectorsSingleton() {};

  public VectorSpace.Vector makeVector(Snippet s) throws IOException {
    return Helpers.makeVectorTokenizingByWhitespace(s.getAsString(), baselineVecspace);
  }
}
