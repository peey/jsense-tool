package in.edu.iiitd.pag.jsense.vss;

import in.edu.iiitd.pag.jsense.snippets.Snippet;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This class combines multiple representations of a snippet and provides an overall scoring function by adding the
 * respective scores if the component score is above a specified cutoff value. If it isn't above the cutoff value,
 * then the score is considered to be 0.
 * @author Peeyush Kushwaha
 */
public class MultipleRepresentations extends SimilarityStrategy {
  List<Pair<SimilarityStrategy, Double>> similarityList;

  /**
   * @param snippets all snippets
   * @param similarityList a list of SimilarityStrategy objects and their cutoffs
   */
  public MultipleRepresentations(Collection<Snippet> snippets, List<Pair<SimilarityStrategy, Double>> similarityList) {
    super(snippets);
    this.similarityList = similarityList;

    /*
    // DEBUG Output: output names of snippets which have empty vectors
    for(Snippet s: snippets) {
      for(VectorSpaceSimilarityStrategy similarity : this.similarityList) {
        if (similarity.jdata.get(s.identifier).isEmpty(0.01)) {
          System.out.printf("%s : %s : %s\n",
              similarity.dvSingleton.getClass().getSimpleName(),
              s.identifier,
              similarity.jdata.get(s.identifier).toString());
        }
      }
    }
    */
  }

  /**
   * @param document1
   * @param document2
   * @return A list of scores between document1 and document 2 per similarity strategy
   * @throws IOException
   */
  List<Double> scoreList(String document1, String document2) throws IOException {
    List<Double> resultList = new ArrayList<>();
    for(Pair<SimilarityStrategy, Double> similarity : this.similarityList) {
      resultList.add(similarity.getLeft().score(document1, document2));
    }
    return resultList;
  }

  /**
   * See {@link MultipleRepresentations} for a description of score calculation methodology
   * @param document1 identifier for the first snippet
   * @param document2 identifier for the second snippet
   * @return score between 0 and n (where n is the size of similarityList)
   * @throws IOException
   */
  @Override
  public Double score(String document1, String document2) throws IOException {
    Double result = .0;
    for(Pair<SimilarityStrategy, Double> similarity : this.similarityList) {
      Double score = similarity.getLeft().score(document1, document2);
      if (score < similarity.getRight()) {
        result = .0;
        break;
      } else {
        result += score;
      }
    }
    return result;
  }
}
