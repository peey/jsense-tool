package in.edu.iiitd.pag.jsense.vss;

import in.edu.iiitd.pag.jsense.snippets.Snippet;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This is an abstract class for different ways for calculating similarity between two documents, no matter what the
 * representation structure (vector, set, etc)
 * @author Peeyush Kushwaha
 */
abstract public class SimilarityStrategy {
  //TODO remove dependence from the class snippet so that this is generalizable. This should be straightforward since
  //we're already using string identifiers everywhere
  public final Collection<Snippet> snippets;

  /**
   * Calculates a similarity score (often between 0 and 1) between two given documents
   * @param document1 identifier for the first snippet
   * @param document2 identifier for the second snippet
   * @return similarity score
   * @throws IOException
   */
  public abstract Double score(String document1, String document2) throws IOException;

  public SimilarityStrategy(Collection<Snippet> snippets) {
    this.snippets = snippets;
  }
}

/**
 * {@link SimilarityStrategy} implementation for finding similarity using set similarity / jaccard similarity.
 * Useful as a potential baseline.
 * @author Peeyush Kushwaha
 */
class JaccardSimilarity extends SimilarityStrategy {
  // cache set representations of the snippets here
  HashMap<String, HashSet> jdata = new HashMap<>();

  public JaccardSimilarity(Map<Snippet, VectorSpace.Vector> dv) {
    super(dv.keySet());

    for (Snippet s : dv.keySet()) {
      HashSet<String> hset = new HashSet<>();
      jdata.put(s.identifier, hset);

      for (String entry : (HashSet<String>) dv.get(s).entries()) {
        if (dv.get(s).get(entry) > 0) {
          hset.add(entry);
        }
      }
    }
  };

  /**
   * @param file1 identifier for document 1
   * @param file2 identifier for document 1
   * @return Jaccard similarity score of two documents
   */
  public Double score(String file1, String file2) {
    HashSet<String> intersection = (HashSet<String>) jdata.get(file2).clone();
    intersection.retainAll(jdata.get(file1));

    HashSet<String> union = (HashSet<String>) jdata.get(file2).clone();
    union.addAll(jdata.get(file1));

    return (double) intersection.size() / union.size();
  }
}

/**
 * {@link SimilarityStrategy} implementation for finding similarity using set generalized jaccard similarity to account
 * for term frequencies. See {@link MultiSetJaccardSimilarity#score(String, String)} for details
 * Useful as a potential baseline.
 * @author Peeyush Kushwaha
 */
class MultiSetJaccardSimilarity extends SimilarityStrategy {

  HashMap<String, Multiset> jdata = new HashMap<>();

  public MultiSetJaccardSimilarity(Map<Snippet, VectorSpace.Vector> dv) {
    super(dv.keySet());

    for (Snippet s : dv.keySet()) {
      Multiset<String> hset = HashMultiset.create();
      jdata.put(s.identifier, hset);

      for (String entry : (HashSet<String>) dv.get(s).entries()) {
        if (dv.get(s).get(entry) > 0) {
          hset.add(entry, (int) dv.get(s).get(entry));
        }
      }
    }
  };

  /**
   * Generalized jaccard index, using term frequency for weights.
   * Also see https://stats.stackexchange.com/a/162650/224638
   * @param file1 identifier for document 1
   * @param file2 identifier for document 1
   * @return Multiset Jaccard similarity score of two documents
   */
  public Double score(String file1, String file2) {
    Multiset<String> intersection = Multisets.intersection(jdata.get(file1), jdata.get(file2));
    Multiset<String> union = Multisets.union(jdata.get(file1), jdata.get(file2));

    return (double) intersection.size() / union.size();
  }
}
