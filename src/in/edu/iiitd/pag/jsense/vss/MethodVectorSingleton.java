package in.edu.iiitd.pag.jsense.vss;

import in.edu.iiitd.pag.jsense.Helpers;
import in.edu.iiitd.pag.jsense.representations.Representations;
import in.edu.iiitd.pag.jsense.snippets.Snippet;

import java.io.IOException;

/**
 * The classes ending in DocumentVectorSingleton describe different ways of representing a code snippet as a document.
 * This is just a wrapper. The main logic is in Representations.java
 * @author Peeyush Kushwaha
 */
public class MethodVectorSingleton extends DocumentVectorsSingleton {
  public static final DocumentVectorsSingleton singleton = new MethodVectorSingleton();

  final VectorSpace<String> methodVecspace = new VectorSpace<String>();

  private MethodVectorSingleton() {};

  public VectorSpace.Vector makeVector(Snippet s) throws IOException {
    String code = s.getWrappedCode();

    String rep = Representations.getMethodsAndFunctionalOperators(code);

    return Helpers.makeVectorTokenizingByWhitespace(rep, methodVecspace);
  }
}
