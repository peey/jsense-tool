package in.edu.iiitd.pag.jsense.vss;

import in.edu.iiitd.pag.jsense.Helpers;
import in.edu.iiitd.pag.jsense.representations.Representations;
import in.edu.iiitd.pag.jsense.snippets.Snippet;

import java.io.IOException;

import static in.edu.iiitd.pag.jsense.API.API.debugOutput;

/**
 * The classes ending in DocumentVectorSingleton describe different ways of representing a code snippet as a document.
 * This is just a wrapper. The main logic is in Representations.java
 * @author Peeyush Kushwaha
 */
public class CFGAllPathsDocumentVectorSingleton extends DocumentVectorsSingleton {

  public static final DocumentVectorsSingleton singleton = new CFGAllPathsDocumentVectorSingleton();
  final VectorSpace<String> CFGAllPathsVecspace = new VectorSpace<String>();

  public VectorSpace.Vector makeVector(Snippet s) throws IOException {
    String code = s.getWrappedCode();

    code = code.replaceAll("public class", "class");

    if (debugOutput) {
      System.out.println(s.identifier);
    }

    String rep = Representations.getAllPathsRepresentation(code, s.identifier);

    VectorSpace.Vector toReturn = Helpers.makeVectorTokenizingByWhitespace(rep, CFGAllPathsVecspace);

    return toReturn;
  }
}
