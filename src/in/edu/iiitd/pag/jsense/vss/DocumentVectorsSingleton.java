package in.edu.iiitd.pag.jsense.vss;

import in.edu.iiitd.pag.jsense.snippets.Snippet;

import java.io.IOException;
import java.util.*;

/**
 * Strategy:
 *  1. Get all snippets from Main
 *  2. Make a vector for each using each available scheme and cache it
 *  3. Return vector from the cache given a snippet on-demand
 *  4. Return collections of vectors when provided a collection of snippets
 *
 *  Alternately to 1 & 2, we could process snippets lazily and just cache them when we see a new one, which is what we're doing currently
 */

// Possible enhancement: change the usage api where user has to first call getDocumentFrequency and then call getIDF manually with the result of the previous call
// Perhaps both results should be returned in the same function call? There shouldn't be two calls if all the user wants is the IDF - a common case

/**
 * This abstract class is extended to  describe different ways of representing a code snippet as a document.
 * @author Peeyush Kushwaha
 */
abstract public class DocumentVectorsSingleton {

  final VectorSpace<String> vecspace = new VectorSpace<String>();

  /**
   * Convenience method on top of {@link DocumentVectorsSingleton#getDocumentFrequency(Collection)}. We can't just simply
   * overload that method because of type erasure.
   * @param snippets
   * @throws Exception
   */
  public HashMap<String, Integer> getDocumentFrequencyForSnippets(Collection<Snippet> snippets) throws Exception {
    Collection vectors = new HashSet();
    // sadly it seems that using Java streams and mapping snippets to vectors doesn't work because the this.getVector mapping function throws an Exception
    for (Snippet snippet : snippets) {
      vectors.add(this.getVector(snippet));
    }

    return getDocumentFrequency(vectors);
  }

  /**
   * Must be overridden by implementing classes. Implementing classes are free to define how they make a vector given a
   * snippet object.
   * @param s a {@link Snippet} object
   * @return Vector for s
   * @throws IOException
   */
  abstract public VectorSpace.Vector makeVector(Snippet s) throws IOException;

  Map<Snippet, VectorSpace.Vector> vectorCache;
  public VectorSpace.Vector getVector(Snippet s) throws IOException {
    if(vectorCache.containsKey(s)) {
      return vectorCache.get(s);
    } else {
      VectorSpace.Vector v = makeVector(s);
      vectorCache.put(s, v);
      return v;
    }
  }

  /**
   * Gets DF from a collection of vectors. This method isn't overidden in inheriting classes.
   * @return HashMap from words to their DF values
   */
  public HashMap<String, Integer> getDocumentFrequency(Collection<VectorSpace.Vector> collection) {
    HashMap<String, Integer> df = new HashMap<>();

    for (VectorSpace.Vector v : collection) {
      for(String word : (HashSet<String>) v.entries()) {
        df.putIfAbsent(word, 0);
        df.put(word, df.get(word) + 1);
      }
    }

    return df;
  }

  /**
   * Gets IDF from the HashMap returned by {@link DocumentVectorsSingleton#getDocumentFrequency(Collection)}.
   * This method isn't overidden in inheriting classes.
   * @return HashMap from words to their IDF values
   */
  public HashMap<String, Double> getIDF(HashMap<String, Integer> df, int totalSnippets) {
    HashMap<String, Double> idf = new HashMap<>();

    for (String word : df.keySet()) {
      idf.put(word, Math.log( (float) totalSnippets / df.get(word)));
    }

    return idf;
  }

  /**
   * Returns a map of vectors with snippets as the corresponding keys for a collection of snippets
   * @param snippets
   * @throws IOException
   */
  public Map<Snippet, VectorSpace.Vector> vectorsForSnippets(Collection<Snippet> snippets) throws IOException {
    //return snippets.stream().collect(Collectors.toMap((Snippet s) -> s.identifier, (Snippet s) -> this.getVector(s)));
    Map<Snippet, VectorSpace.Vector> map = new HashMap<>();

    for (Snippet s: snippets) {
      map.put(s, this.getVector(s));
    }

    return map;
  }

  public DocumentVectorsSingleton() {
    vectorCache = new HashMap<>();
  }
}

