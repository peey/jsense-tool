package in.edu.iiitd.pag.jsense.API;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Used for pretty printing detailed information about comparison of two snippets. Holds scores for comparison using
 * all applicable representations.
 * @author Peeyush Kushwaha
 */
public class SnippetComparisonResult {
  public String name1;
  public String name2;

  public Double baselineScore;
  public Double methodScore;
  public Double typeScore;
  public Double CFGAllPathsScore;
  public Double combinedScore;

  //https://stackoverflow.com/a/1526987/
  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }
}
