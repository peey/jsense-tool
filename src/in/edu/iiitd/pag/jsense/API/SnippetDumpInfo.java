package in.edu.iiitd.pag.jsense.API;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Used for pretty printing detailed information about a snippet
 * @author Peeyush Kushwaha
 */
public class SnippetDumpInfo {
  public String name;
  public boolean isCompilable;
  public String methodRepresentation;
  public String typeRepresentation;
  public String CFGAllPathsRepresentation;

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
  }
}
