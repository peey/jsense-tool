package in.edu.iiitd.pag.jsense.API;

import in.edu.iiitd.pag.jsense.Helpers;
import in.edu.iiitd.pag.jsense.representations.Representations;
import in.edu.iiitd.pag.jsense.snippets.Snippet;
import in.edu.iiitd.pag.jsense.snippets.Snippets;
import in.edu.iiitd.pag.jsense.vss.*;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Contains a user's interface to the tool. High level overview is that user should be able to compare two snippets and
 * get a score between them
 * @author Peeyush Kushwaha
 */
public class API {
  /**
   * set to true if you want to see output of checker framework, compilability tests, etc
   */
  public static boolean debugOutput = false;

  private static boolean snippetsInitialized = false;
  private static SimilarityStrategy strategy;

  /**
   * Loads snippets from the given directory path and caches them for subsequent processing
   * @param path
   * @param regex Regex to match filenames
   */
  private static void initializeAllSnippets(String path, String regex) {
    if(snippetsInitialized) throw new IllegalStateException("initializeAllSnippets called twice. This shouldn't happen. Uninitialize first.");

    Snippets.loadSnippetsFromDir(path, regex);
    snippetsInitialized = true;
  }

  /**
   * The representations which have to be used. This list can be modified to give better results.
   */
  private static List<DocumentVectorsSingleton> representations;
  static {
    representations = List.of(
      BaselineDocumentVectorsSingleton.singleton,
      MethodVectorSingleton.singleton,
      TypeVectorSingleton.singleton,
      CFGAllPathsDocumentVectorSingleton.singleton);
  }

  private static List<SimilarityStrategy> strategies;

  /**
   * Extracts representations from the loaded snippets and caches then for subsequent querying
   * @throws IOException
   */
  private static void initializeStrategies() throws IOException {
    if (strategies != null) throw new IllegalStateException("Initializing strategies twice, this shouldn't happen. Uninitialize first.");

    strategies = new ArrayList<>();

    Collection<Snippet> snippets = Snippets.getAllCompilableSnippets();
    for (DocumentVectorsSingleton dvSingleton : representations) {
      Map<Snippet, VectorSpace.Vector> vectors = dvSingleton.vectorsForSnippets(snippets);
      VectorSpaceSimilarity strategy = new VectorSpaceSimilarity(vectors, dvSingleton);
      strategies.add(strategy);
    }

    MultipleRepresentations multiStrategy = new MultipleRepresentations(snippets, List.of(
        Pair.of(strategies.get(1), .0),
        Pair.of(strategies.get(2), .1),
        Pair.of(strategies.get(3), .0)));

    strategies.add(multiStrategy);
  }

  /**
   * Can be used to control debug output
   * @param b false for supressing debug output, true for enabling it
   */
  public static void setVerbosity(boolean b) {
    debugOutput = b;
  }

  /**
   * Internally calls initializeAllSnippets and then initializeAllStrategies, with error reporting capabilities
   * @param path
   * @param regex
   * @throws IOException
   */
  public static void initialize(String path, String regex) throws IOException {
    try {
      if(!debugOutput) Helpers.supressOP();
      initializeAllSnippets(path, regex);
      initializeStrategies();
      if(!debugOutput) Helpers.resumeOP();
    } catch (Exception e) {
      if(!debugOutput) {
        Helpers.resumeOP();
        Helpers.fulshSupressedOP();
      }
      e.printStackTrace();
      System.out.println("failed to initialize");
      System.exit(1);
    }
  }

  /**
   * Resets the initialized snippets and their representations. Must be called before trying to load snippets
   * from another directory
   */
  public static void uninitialize() {
    snippetsInitialized = false;
    strategies = null;
  }

  /**
   * @see API#initialize(String, String)
   * @param path
   * @throws IOException
   */
  public static void initialize(String path) throws IOException {
    initialize(path, ".*");
  }

  /**
   * Returns scores between two given snippets using different strategies (including baseline strategy), and also gives
   * a combined score which is the mean score of all our strategies
   * @param file1
   * @param file2
   * @return
   * @throws IOException
   */
  public static SnippetComparisonResult compareSnippets(String file1, String file2) throws IOException {
    if(!isCompilable(file1)) throw new IllegalArgumentException(file1 + " isn't compilable");
    if(!isCompilable(file2)) throw new IllegalArgumentException(file2 + " isn't compilable");

    SnippetComparisonResult r = new SnippetComparisonResult();


    List<Double> scores = new ArrayList<>();
    Collection<Snippet> snippets = Snippets.getAllSnippets();
    for (SimilarityStrategy strategy : strategies) {
      scores.add(strategy.score(file1, file2));
    }

    r.name1 = file1;
    r.name2 = file2;
    //WARNING: RELIES ON ordering of things in the array. WIll break easily
    r.baselineScore = scores.get(0);
    r.methodScore = scores.get(1);
    r.typeScore = scores.get(2);
    r.CFGAllPathsScore = scores.get(3);
    r.combinedScore = scores.get(4) / 3;
    return r;
  }

  /**
   * Checks compilability of a snippet
   * @param file1 filename of the snippet
   * @return
   * @throws IOException
   */
  public static boolean isCompilable(String file1) throws IOException {
    return Snippets.getSnippetMatching(file1).compilable();
  }

  /**
   * Get all information about a snippet. Includes all applicable representations, compilability status and name of the
   * snippet
   * @param file
   * @return
   * @throws IOException
   */
  public static SnippetDumpInfo dumpSnippetInfo(String file) throws IOException {
    SnippetDumpInfo result = new SnippetDumpInfo();
    Snippet s = Snippets.getSnippetMatching(file);
    result.name = s.identifier;
    result.isCompilable = s.compilable();
    result.methodRepresentation = Representations.getMethodsAndFunctionalOperators(s.getWrappedCode());
    result.typeRepresentation = Representations.getTypes(s.getWrappedCode());
    if (result.isCompilable) {
      if (!debugOutput) Helpers.supressOP();
      result.CFGAllPathsRepresentation = Representations.getAllPathsRepresentation(s.getWrappedCode());
      if (!debugOutput) Helpers.resumeOP();

    }
    return result;
  }

  public static void main(String args[]) throws IOException{
    initialize("../jsense/data/factorial-study");
    System.out.println(compareSnippets("1_56.java", "1_52.java"));
    System.out.println(dumpSnippetInfo("1_56.java"));
    System.out.println(dumpSnippetInfo("1_52.java"));
  }

}
