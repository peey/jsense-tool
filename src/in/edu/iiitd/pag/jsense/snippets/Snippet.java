package in.edu.iiitd.pag.jsense.snippets;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The purpose of the Snippets module is to handle the files and load them in a way that can be handled by the rest of
 * the system.
 * A lot of methods on the snippets are provided for convenience and actually delegate the work to methods of other
 * classes.
 * @author Peeyush Kushwaha
 */
public class Snippet implements Comparable {
  private File file;
  public String identifier;
  // once we load the contents of the file, we cache it so that we don't have to read from the disk again and again
  private String fileContents = "";
  // wrappedcode is the cache for getWrappedCode() function
  private String wrappedCode = "";
  // this is cached value for compilable() function
  private Boolean compiles;

  /**
   * Specify a File object where snippet can be found and an identifier which is useful for grouping snippets by topics
   * (see {@link Snippets#getSnippetsMatching(String)}) and not relying on filename as identifier in maps, as file names
   * could be similar.
   * @param file
   * @param identifier
   */
  public Snippet(File file, String identifier) {
    this.file = file;
    this.identifier = identifier;
  }

  /**
   * This method also caches the file and on subsequent calls return the cached values
   * @return snippet file's contents as a string
   * @throws IOException
   */
  public String getAsString() throws IOException {
    if (fileContents.isEmpty()) {
      fileContents = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
    }
    return fileContents;
  }


  /**
   * By default, for our data we used the pattern "topic<id>" for identifiers. This extracts the numeric id from the
   * identifier
   * @return numeric id from snippet identifier
   */
  public Integer snippetID() {
    Pattern pattern = Pattern.compile("[a-z-]+<([0-9]+)>");
    Matcher m = pattern.matcher(identifier);
    if (m.matches()) {
      String match = m.group(1);
      return  Integer.valueOf(match);
    } else {
      return -1;
    }
  }

  /**
   * A file name is required in {@link Compilability#checkIfCompiles(javax.tools.JavaFileObject)}, so we make up one
   * based on the snippet identifier. The actual code checked for compilability is from {@link Snippet#getWrappedCode()}
   * so it doesn't exist in any real file.
   * @return .java file name based on identifier
   */
  public String pseudoFileName() {
    return identifier.replaceAll("[^A-Za-z0-9_]", "_").replaceAll("__+", "_") + ".java";
  }

  /**
   * All our collected snippets are method level. So we wrap them in a class in order to use them with other tools like
   * javac and checker framework
   * @return method level code wrapped in a Java class
   * @throws IOException
   */
  public String getWrappedCode() throws IOException{
    if(!this.wrappedCode.equals("")) {
      return this.wrappedCode;
    } else {
      this.wrappedCode = Compilability.getWrappedCode(this);
      return this.wrappedCode;
    }
  }

  /**
   * Checks if snippet compiles or not using {@link Compilability}
   * @return true if snippet is compilable
   * @throws IOException
   */
  public boolean compilable() throws IOException {
    if(compiles != null) {
      return compiles;
    } else {
      compiles = Compilability.checkIfCompiles(this);
      return compiles;
    }
  }

  /**
   * For pretty printing a snippet's identifier
   */
  @Override
  public String toString() {
    return "Snippet[" + identifier + "]";
  }

  /**
   * We consider two snippet objects with same identifer to be equal. This and {@link Snippet#hashCode()} are useful for
   * using {@link Snippet} objects as HashMap keys, in HashSets etc.
   * @param obj
   */
  @Override
  public boolean equals(Object obj) {
    return obj.toString().equals(toString());
  }

  /**
   * See documentation for {@link Snippet#equals(Object)}
   */
  @Override
  public int hashCode() {
    return identifier.hashCode();
  }

  /**
   * Useful for ordering snippets by their numeric IDs
   */
  @Override
  public int compareTo(Object o) {
    if (o instanceof Snippet)  {
      return snippetID().compareTo(((Snippet) o).snippetID());
    } else {
      return 0;
    }
  }
}

