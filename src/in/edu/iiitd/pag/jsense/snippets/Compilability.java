package in.edu.iiitd.pag.jsense.snippets;

import javax.lang.model.element.Element;
import javax.tools.*;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.util.JavacTask;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.*;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * For checking if a code snippet is compilable or not. Note that this currently doesn't use the Snippet class so
 * it replicates some of the functionality already present there. This could be changed. The independence, however means
 * that this class could work on its own without rest of the system. Just one big compilability checker.
 * @author Peeyush Kushwaha
 */
class Compilability {

  /**
   * Get compilable code from a code fragment (which may contain just a method, for instance)
   * @see Snippet#getWrappedCode() for use cases
   * @see Compilability#getASTNode(String) for implemnetation
   * @param codeFragment partial code fragment
   * @return code wrapped in a class (and even method if needed)
   */
  public static String getWrappedCode(String codeFragment) {
    return getASTNode(codeFragment).toString();
  }

  /**
   * Same as {@link Compilability#getWrappedCode(String)} for a snippet's file contents
   */
  public static String getWrappedCode(Snippet s) throws IOException {
    return getWrappedCode(s.getAsString());
  }

  /**
   * Internal method. Converts code in string to a file object as expected by the javac compiler.
   */
  public static JavaFileObject fileObjectFromCode(String code, String name) {
    return new SimpleJavaFileObject(URI.create("myfo:/" + name), JavaFileObject.Kind.SOURCE) {
      public CharSequence getCharContent(boolean ignoreEncodingErrors) {
        return code;
      }
    };
  }

  public static String getFileContents(File file) throws IOException {
    return new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
  }

  /**
   * Usage Example:
   * <pre>
   * {@code String code = "public static void main(String[] args) {System.out.println(\"World\");}";
   * JavaFileObject fo = fileObjectFromCode(getWrappedCode(code), "fakename.java");
   * checkIfCompiles(fo);
   * }
   * </pre>
   * @param fo
   * @return true if code compiles
   * @throws IOException
   */
  public static boolean checkIfCompiles(JavaFileObject fo) throws IOException {

    java.util.List<JavaFileObject> files = Arrays.asList(fo);
    JavaCompiler tool = ToolProvider.getSystemJavaCompiler();

    StandardJavaFileManager fileManager = tool.getStandardFileManager(null, null, null);

    StringBuilder classPathSB = new StringBuilder(); // you may or may not allow snippets to use certain libraries.
    // put the paths for their jar files here if you want to

    /*
    String prefix =  "./common-libs/";

    File[] libs = new File(prefix).listFiles();


    for (int i = 0; i < libs.length; i++) {
      classPathSB.append(prefix);
      classPathSB.append(libs[i].getName());
      classPathSB.append(":");
    }
    */

    JavacTask ct = (JavacTask)tool.getTask(null, fileManager, null, Arrays.asList("-cp", classPathSB.toString()), null, files);

    Iterable<? extends CompilationUnitTree> compUnits = ct.parse();

    Iterable<? extends Element> analysis = ct.analyze();

    return analysis.iterator().hasNext();
  }

  /**
   * Overloaded convenience method for snippet objects
   * @see Compilability#checkIfCompiles(JavaFileObject)
   */
  public static boolean checkIfCompiles(Snippet s) throws IOException {
    JavaFileObject jfo = fileObjectFromCode(getWrappedCode(s.getAsString()), s.pseudoFileName());
    return checkIfCompiles(jfo);
  }

  // Internally used in detectCodeType and getASTNode
  private enum CodeType {
    PACKAGE, CLASS, STATEMENTS, METHOD
  }

  private static ASTParser parser;

  static boolean initialized = false;

  /**
   * Internal method. Initializes the eclipse jdt parser
   */
  public static void init() {
    if (!initialized) {
      initialized = true;
      parser =  ASTParser.newParser(AST.JLS8);
      Map options = JavaCore.getOptions();
      JavaCore.setComplianceOptions(JavaCore.VERSION_1_8, options);
      parser.setCompilerOptions(options);
      parser.setResolveBindings(false);
    }
  }

  /**
   * Detect what level the partial codeFragment is at so that we can wrap it accordingly
   * @param parser
   * @param codeFragment
   * @return
   */
  public static CodeType detectCodeType(ASTParser parser, String codeFragment) {
    init(); //FIXME: why do we need to call init here? Can we assume that the parser passed as argument is already initialized?
    parser.setSource(codeFragment.toCharArray());
    boolean isClassOrStatements = false;
    ASTNode node = null;

    try {
      parser.setKind(ASTParser.K_STATEMENTS);
      // works for a class w/o imports or package declarations
      parser.setSource(codeFragment.toCharArray());
      node = (Block) parser.createAST(null);
      isClassOrStatements = !node.toString().trim().equalsIgnoreCase("{\n}");
    } catch (Exception e) {
      isClassOrStatements = false;
    }

    if (isClassOrStatements) {
      class MethodDeclarationChecker extends ASTVisitor {
        public boolean hasMethodDeclaration = false;
        @Override
        public boolean visit(MethodDeclaration node) {
          this.hasMethodDeclaration = true;
          return false;
        }
      }

      MethodDeclarationChecker v = new MethodDeclarationChecker();

      node.accept(v);

      return v.hasMethodDeclaration? CodeType.CLASS : CodeType.STATEMENTS;
    } else {
      parser.setKind(ASTParser.K_COMPILATION_UNIT);
      parser.setSource(codeFragment.toCharArray());
      parser.setResolveBindings(false);
      node = (CompilationUnit) parser.createAST(null);
      if (node.toString().trim().equalsIgnoreCase("{\n}") || node.toString().trim().equalsIgnoreCase("")) {
        return CodeType.METHOD;
      } else {
        return CodeType.PACKAGE;
      }
    }
  }

  /**
   * Parses given codeFragemnt string (wrapping with a dummy class if needed) using jdt tools and returns an ASTNode.
   * @param codeFragment
   */
  public static ASTNode getASTNode(String codeFragment) {
    init();
    parser.setSource(codeFragment.toCharArray());
    ASTNode node = null;

    String imports = "import java.*; \n" +
        "import java.util.*; \n" +
        "import java.math.*; \n" +
        "import java.io.*; \n" +
        "import java.nio.*; \n" +
        "import java.nio.file.*; \n" +
        "import java.nio.charset.*; \n" +
        "import java.awt.*; \n" +
        "import javax.swing.*; \n" +
        "import java.text.*; \n" +
        "import java.util.regex.*; \n" +
        "import java.util.Scanner; \n" +
        "import java.nio.file.Files; \n" +
        "import java.util.List; \n";


    switch(detectCodeType(parser, codeFragment)) {
      //NOTE: this falls through
      case STATEMENTS: codeFragment = "void m() {" + codeFragment + "}";
      case METHOD: codeFragment = "class A {" + codeFragment + "}";
      case CLASS: codeFragment = imports + codeFragment;
    }

    parser.setKind(ASTParser.K_STATEMENTS);
    try {
      // works for a class w/o imports or package declarations
      parser.setSource(codeFragment.toCharArray());
      node = (Block) parser.createAST(null);
    } catch (Exception e) {

      // this works with classes, and statements not wrapped in a method
      parser.setSource(codeFragment.toCharArray());
      node = (Block) parser.createAST(null);
    }

    // this works for methods and packages
    if (node.toString().trim().equalsIgnoreCase("{\n}")) {
      parser.setKind(ASTParser.K_COMPILATION_UNIT);
      parser.setSource(codeFragment.toCharArray());
      parser.setResolveBindings(false);
      node = (CompilationUnit) parser.createAST(null);
      if (node.toString().trim().equalsIgnoreCase("{\n}")
          || node.toString().trim().equalsIgnoreCase("")) {
        codeFragment = " class A { \n" + codeFragment + "\n }";

        parser.setKind(ASTParser.K_COMPILATION_UNIT);
        parser.setSource(codeFragment.toCharArray());
        parser.setResolveBindings(false);
        node = (CompilationUnit) parser.createAST(null);
      }
    }

    boolean hasMethodDeclaration = false;

    class MethodDeclarationChecker extends ASTVisitor {
      public boolean hasMethodDeclaration = false;
      @Override
      public boolean visit(MethodDeclaration node) {
        this.hasMethodDeclaration = true;
        return false;
      }
    }
    MethodDeclarationChecker v = new MethodDeclarationChecker();

    node.accept(v);

    // this works for method bodies (statements not wrapped in a method)
    if (!v.hasMethodDeclaration) {
      parser.setKind(ASTParser.K_STATEMENTS);
      codeFragment = "class A {void c() {" + codeFragment + "}}";
      parser.setSource(codeFragment.toCharArray());
      node = parser.createAST(null);
    }

    return node;
  }

  /**
   * Checks if codeFragment is parsable
   * @param codeFragment
   * @return true if parsable
   */
  public static boolean checkIfParses(String codeFragment) {
    try {
      getASTNode(codeFragment);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  private enum CompileParseStatus {
    compiles, parses, doesnt_parse
  }

  /**
   * Analyze all snippets in directory and find which ones are parsable, which ones are compilable and print as well as
   * return a summary.
   * @param pathToDirectory
   * @param pattern
   * @return List of triplets (File, CodeType, CompileParseStatus) returned as pair(File, pair(CodeType, CompileParseStatus)
   * @throws IOException
   */
  public static List<Pair<File, Pair<CodeType, CompileParseStatus>>> checkCompilableSnippetsInDirectory(String pathToDirectory, Pattern pattern) throws IOException {
    File[] files = new File(pathToDirectory).listFiles();
    List<Pair<File, Pair<CodeType, CompileParseStatus>>> result = new ArrayList<>();

    Integer totalParsing = 0, totalCompiling = 0, totalMatches = 0;

    for (File file : files) {
      String name = file.getName();
      Matcher matcher = pattern.matcher(name);
      String codeFragment = getFileContents(file);
      if (matcher.find()) {
        totalMatches++;
        boolean parses = false, compiles = false;
        if (checkIfParses(codeFragment)) {
          parses = true;
          JavaFileObject fo = fileObjectFromCode(getWrappedCode(codeFragment).replaceAll("public class", "class"), file.getName());
          compiles = checkIfCompiles(fo);
        }

        if (parses) totalParsing++;
        if (compiles) totalCompiling++;

        CodeType codeType = detectCodeType(parser, codeFragment);
        CompileParseStatus status = compiles? CompileParseStatus.compiles : parses? CompileParseStatus.parses : CompileParseStatus.doesnt_parse;
        System.out.printf("Processing %25s. \t Code Type Detected : %10s. \t Status : %10s \n", file.getName(), codeType, status);
        result.add(Pair.of(file, Pair.of(codeType, status)));
      } else {
        System.out.printf("FileName %s doesn't match specified pattern \n", file.getName());
      }
    }

    System.out.printf("%d / %d are file names which match the pattern \n", totalMatches, files.length);
    System.out.printf("%d / %d are files which are parsable \n", totalParsing, totalMatches);
    System.out.printf("%d / %d are files which are compilable \n", totalCompiling, totalMatches);

    return result;
  }

  public static void main(String[] args) throws IOException {
    /*
    String directoryPath = args[0];
    Pattern pattern = Pattern.compile(args[1]);
    */

    String directoryPath = "/mnt/software/hack/jsense/data/reversestring-study";
    Pattern pattern = Pattern.compile("rev_([0-9]+).java");

    List<Pair<File, Pair<CodeType, CompileParseStatus>>> result = checkCompilableSnippetsInDirectory(directoryPath, pattern);
  }
}
