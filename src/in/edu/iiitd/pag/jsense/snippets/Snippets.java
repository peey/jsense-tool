package in.edu.iiitd.pag.jsense.snippets;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * This class aids in managing a collection of snippets within a directory
 * @author Peeyush Kushwaha
 */
public class Snippets {

  // Caching results so we don't have to open multiple file resources for the same snippet. Unsure show Java handles this
  // internally, I'm assuming it opens a new file resource on every new File call
  static Collection<Snippet> allSnippetsMemoized;

  /**
   * Get all snippets in directory `path` and store them using their names as identifiers. Loads them into static
   * allSnippetsMemoized variable
   * @param path director to scan for snippet files
   * @param regex filter for file names. Pass in ".*" for no filters
   */
  public static void loadSnippetsFromDir(String path, String regex) {
    ArrayList<Snippet> result = new ArrayList<>();

    File[] files = new File(path).listFiles();

    for (File file : files) {
      String name = file.getName();
      if (name.matches(regex)) {
        result.add(new Snippet(file, name)); //using full file name as the identifier
      } else {
        System.out.println("Warning: a file which does not match the given regex - " + file.getName());
      }
    }

    allSnippetsMemoized = result;
  }

  /**
   * Please call loadSnippetsFromDir before calling this method
   * @return collection of snippets initialized via {@link Snippets#loadSnippetsFromDir(String, String)}
   */
  public static Collection<Snippet> getAllSnippets() {
    if (allSnippetsMemoized == null || allSnippetsMemoized.isEmpty()) {
      throw new IllegalStateException("Please initialize all snippets before attempting to fetch them");
    } else {
      return allSnippetsMemoized;
    }
  }

  /**
   * Collection of compilable snippets out of {@link Snippets#getAllSnippets()}
   * @throws IOException
   */
  public static Collection<Snippet> getAllCompilableSnippets() throws IOException  {
    return getAllSnippets().stream().filter(snippet -> {
      try {
        return snippet.compilable();
      } catch (IOException e) {
        return false;
      }
    }).collect(Collectors.toSet());
  }

  /**
   * Filter snippets from {@link Snippets#getAllSnippets()} by identifier
   * @param pattern the regex pattern to match
   */
  public static Collection<Snippet> getSnippetsMatching(String pattern) {
    Pattern restrictPattern = Pattern.compile(pattern);
    Collection<Snippet> result = getAllSnippets();
    return result.stream().filter(snippet -> snippet.identifier.matches(restrictPattern.pattern())).collect(Collectors.toList());
  }

  /**
   * Get a single snippet whose identifier matches specified regex pattern. Useful for individually getting a snippet by
   * identifier
   * @param pattern the regex pattern to match
   * @return
   */
  public static Snippet getSnippetMatching(String pattern) {
    //TODO better implementation - the memoized version should be a map
    List<Snippet> allMatching = (new ArrayList<Snippet>(getSnippetsMatching(pattern)));
    if (allMatching.isEmpty()) throw new IllegalArgumentException("There is no such snippet: " + pattern);
    return allMatching.get(0);
  }
}

